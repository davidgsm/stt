#-\ *- coding: utf-8 -*-

import smbus			
import funciones
import time  
import csv

#tiempo data giro en segundos
t_giro = 10

#Registros MPU6050 a utilizar
PWR_MGMT_1   = 0x6B
SMPLRT_DIV   = 0x19
CONFIG       = 0x1A
GYRO_CONFIG  = 0x1B
INT_ENABLE   = 0x38
GYRO_XOUT_H  = 0x43
GYRO_YOUT_H  = 0x45
GYRO_ZOUT_H  = 0x47

bus = smbus.SMBus(1) 	
Device_Address = 0x68   #Direccion-MPU

#Angulos iniciales en 0
roll_gyro = roll_gyro_ant = 0
pitch_gyro = pitch_gyro_ant = 0
yaw_gyro = yaw_gyro_ant = 0

#sensivilidad-escalado
sg1 = 131 #+-250 degree
sg2 = 65.5 #+-500 degree
sg3 = 32.8 #+-1000 degree
sg4 = 16.4 #+-2000 degree

global a

a = sg3 

funciones.MPU_Init(bus, Device_Address, SMPLRT_DIV, PWR_MGMT_1, CONFIG, GYRO_CONFIG, INT_ENABLE)

#Calibracion, promediando valores en reposo para obtener el offset
funciones.cal(a, bus, Device_Address, GYRO_XOUT_H, GYRO_YOUT_H, GYRO_ZOUT_H)
print("Offset Calculado")

print ("Angulos Roll Pitch Yaw")


timer = time.time()
timer_2 = time.time()
t_stop = 0
data = open("data.txt", "w")

while t_giro > t_stop:
	  t_stop = time.time() - timer_2
	  #Valores en bruto de giroscopio y acelerometro
	  gyro_x = funciones.read_raw_data(bus, Device_Address, GYRO_XOUT_H)
	  gyro_y = funciones.read_raw_data(bus, Device_Address, GYRO_YOUT_H)
	  gyro_z = funciones.read_raw_data(bus, Device_Address, GYRO_ZOUT_H)
		
	  #Sensibilidad
	  Gx = (gyro_x/a) - (funciones.GxCal)
	  Gy = (gyro_y/a) - (funciones.GyCal)
	  Gz = (gyro_z/a) - (funciones.GzCal)
	 	
	  dt = time.time() - timer 
	  timer = time.time()
	
	  roll_gyro = roll_gyro + (roll_gyro_ant + Gx)*dt
	  roll_gyro_ant = Gx
	  
	  pitch_gyro = pitch_gyro + (pitch_gyro_ant + Gy)*dt
	  pitch_gyro_ant = Gy
	  
	  yaw_gyro = yaw_gyro + (yaw_gyro_ant + Gz)*dt
	  yaw_gyro_ant = Gz
	  
	  roll = "%.2f" %roll_gyro 
	  pitch = "%.2f" %pitch_gyro
	  yaw = "%.2f" %yaw_gyro
	  angle_data = str(roll) + ' ' + str(pitch) + ' ' + str(yaw) + ' \r\n'
	  data.write(angle_data)
	  
	  #print(roll, pitch, yaw)
	  print ("Roll = {0}º  Pitch = {1}º  Yaw = {2}º".format(roll,pitch,yaw))
