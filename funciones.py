#funciones

def MPU_Init(bus, Device_Address, SMPLRT_DIV, PWR_MGMT_1, CONFIG, GYRO_CONFIG, INT_ENABLE):
	#Escribir en los registros
	bus.write_byte_data(Device_Address, SMPLRT_DIV, 7)
	bus.write_byte_data(Device_Address, PWR_MGMT_1, 1) 
	bus.write_byte_data(Device_Address, CONFIG, int('0000110',2)) 
	bus.write_byte_data(Device_Address, GYRO_CONFIG, 24)
	bus.write_byte_data(Device_Address, INT_ENABLE, 1)

def read_raw_data(bus, Device_Address, addr):
	#Accelero and Gyro value are 16-bit
        high = bus.read_byte_data(Device_Address, addr)
        low = bus.read_byte_data(Device_Address, addr+1)
    
        #concatenate higher and lower value
        value = ((high << 8) | low)
        
        #to get signed value from mpu6050
        if(value > 32768):
                value = value - 65536
        return value

def cal(a, bus, Device_Address, GYRO_XOUT_H, GYRO_YOUT_H, GYRO_ZOUT_H):
		
	print("Calculando offset")
	global GxCal, GyCal, GzCal
	
	GxCal = GyCal = GzCal = 0.0
	x = y = z = 0.0
	
	for i in range(500):
		x = x + read_raw_data(bus, Device_Address, GYRO_XOUT_H)
		y = y + read_raw_data(bus, Device_Address, GYRO_YOUT_H)
		z = z + read_raw_data(bus, Device_Address, GYRO_ZOUT_H)
	x = x/500
	y = y/500
	z = z/500
	GxCal = x/a
	GyCal = y/a
	GzCal = z/a
	print("Offset X =%.2f" %GxCal, "offset y=%.2f" %GyCal, "Offset z=%.2f" %GzCal)
	return GxCal, GyCal, GzCal
	
"""	
#angulos desde acelerometro
def dist(math,a,b):
    return math.sqrt((a*a)+(b*b))

def get_pitch(math,x,y,z):
    radians = math.atan2(-x, dist(math,y,z))
    return math.degrees(radians)
 
def get_roll(math,x,y,z):
    radians = math.atan2(y, dist(math,x,z))
    return math.degrees(radians)
    """
