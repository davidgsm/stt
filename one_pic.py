#!/usr/bin/env python2
import os
import time 
from PIL import Image



#set tiempo
t = 200

time_mili_s = t
time_micro_s = 1000 * time_mili_s
current_time = time.strftime("%d_%m_-_%H_%M_%S", time.localtime())
init_dir = '/home/pi/' + current_time
pic_name = '{}_time_in_ms_{}.jpg'.format(init_dir, time_mili_s)
task = 'raspistill -w 1024 -h 1024 -o {} -t 1 -ss {}'.format(pic_name, time_micro_s)
print(task)
os.system(task)

#validar imagen
im = Image.open(pic_name)
im1L = im.convert('L')

#pasa la imagen a una matriz 1024x1024
im1_array = np.array(im1L)
columna , fila = im1L.size


a_im1 = []
for i in range(columna):
    b=0
    for j in range(fila):
        b = b + im1_array[j, i]
    a_im1.append(b)

peak = []
e_x = []
for k in range(len(a_im1)):
    if k==0: 
        if a_im1[k] > a_im1[k+1]:
            peak.append(a_im1[k])
            e_x.append(k)
        else:
            pass
    if k>0 and k<(len(a_im1)-1):
        if a_im1[k] > a_im1[k-1] and a_im1[k] > a_im1[k+1]:
            peak.append(a_im1[k])
            e_x.append(k)
        else:
            pass
    if k == (len(a_im1) - 1):
        if a_im1[k] > a_im1[k-1]:
            peak.append(a_im1[k])
            e_x.append(k)
        else:
            pass

p = filter(lambda x: x > max(a_im1)*0.2, peak)
l_p = list(p)

print("peaks detectados: ", len(l_p))

if len(l_p) > 5:
    print("valid image, peaks detected: ", l_p)
else:
    print("it's not a valid image")
